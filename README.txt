DESCRIPTION
===========

The mailwire modules wires together any number of mail formatting and sending modules. If you want to use mimemail, maillog and the smtp module at the same time, mailwire is your friend.

Please note, that mailwire does nothing by itself. It's an API module that serves as registry for all mail modules.

Mail modules can register any number of mail formatters and sending classes. Every registry entry can have a module, key and weight property. This properties are evaluated every time drupal_mail is called.

Depending on the registry entries the right formatters and sending classes are called.

Mailwire registers itself via the mail_system variable. No other mail module should touch this variable, they should register themselves in the mailwire registry in order to be called in the right order.


INSTALATION
===========

Place the entirety of this directory in sites/all/modules/mailwire.

That's all you have to do. Just enable the other mail modules which are based on mailwire to make them to work together.


API USAGE
=========

See the documentation of hook_mailwire_sender() and hook_mailwire_formatter() in mailwire.module.