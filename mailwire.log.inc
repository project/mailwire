<?php

function _mailwire_log($message, $level) {
	switch ($level) {
		case WATCHDOG_ERROR:
		case WATCHDOG_WARNING:
			drupal_set_message($message, 'error');
			break;
		case WATCHDOG_NOTICE:
			drupal_set_message($message, 'warning');
			break;
			case WATCHDOG_INFO:
			drupal_set_message($message, 'status');
	}
	watchdog('mailwire', $message, NULL, $level);
}

