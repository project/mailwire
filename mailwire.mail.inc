<?php

/**
 * This should be the only Drupal 7 mail system set in the mail_system variable.
 */
class MailwireMailSystem implements MailSystemInterface {
	
  /**
   * Format a message composed by drupal_mail() prior to sending.
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return
   *   The formatted $message.
   */
  public function format(array $message) {
		$caller = MailwireMailSystem::findCallingModule();
		// order of relevance:
		// 1. module, key, weight
		// 2. module, wheight
		// 3. weight
		$formatters = array();
		$dbresult = db_select('mailwire_formatter', 'f')
			->fields('f')
			->condition('f.key', $caller->key, '=')
			->condition('f.module', $caller->module, '=')
			->orderBy('f.weight')
			->execute();
		foreach ($dbresult as $record) {
			$formatters[] = $record->class_name;
		}
			
		$dbresult = db_select('mailwire_formatter', 'f')
			->fields('f')
			->condition('f.module', $caller->module, '=')
			->orderBy('f.weight')
			->execute();
		foreach ($dbresult as $record) {
			$formatters[] = $record->class_name;
		}
										
		$dbresult = db_select('mailwire_formatter', 'f')
		  ->fields('f')
			->orderBy('f.weight')
			->execute();
		foreach ($dbresult as $record) {
			$formatters[] = $record->class_name;
		}
		
		if (count($formatters)==0) {
			$formatter = new DefaultMailSystem();
			return $formatter->format($message);
		}
		else {
			foreach ($formatters as $formatter_class) {
				$formatter = new $formatter_class();
				$message = $formatter->format($message);
			}
		}
		
		return $message;
  }

  /**
   * Send a message composed by drupal_mail().
   *
   * @param $message
   *   Message array with at least the following elements:
   *   - id: A unique identifier of the e-mail type. Examples: 'contact_user_copy',
   *     'user_password_reset'.
   *   - to: The mail address or addresses where the message will be sent to.
   *     The formatting of this string must comply with RFC 2822. Some examples:
   *     - user@example.com
   *     - user@example.com, anotheruser@example.com
   *     - User <user@example.com>
   *     - User <user@example.com>, Another User <anotheruser@example.com>
   *    - subject: Subject of the e-mail to be sent. This must not contain any
   *      newline characters, or the mail may not be sent properly.
   *    - body: Message to be sent. Accepts both CRLF and LF line-endings.
   *      E-mail bodies must be wrapped. You can use drupal_wrap_mail() for
   *      smart plain text wrapping.
   *    - headers: Associative array containing all additional mail headers not
   *      defined by one of the other parameters.  PHP's mail() looks for Cc
   *      and Bcc headers and sends the mail to addresses in these headers too.
   *
   * @return
   *   TRUE if the mail was successfully accepted for delivery, otherwise FALSE.
   */
  public function mail(array $message) {
		$caller = MailwireMailSystem::findCallingModule();
		// order of relevance:
		// 1. module, key, weight
		// 2. module, wheight
		// 3. weight
		$senders = array();
		$dbresult = db_select('mailwire_sender', 's')
		  ->fields('s')
			->condition('s.key', $caller->key, '=')
			->condition('s.module', $caller->module, '=')
			->orderBy('s.weight')
			->execute();
		foreach ($dbresult as $record) {
			$senders[] = $record->class_name;
		}
			
		$dbresult = db_select('mailwire_sender', 's')
		  ->fields('s')
			->condition('s.module', $caller->module, '=')
			->orderBy('s.weight')
			->execute();
		foreach ($dbresult as $record) {
			$senders[] = $record->class_name;
		}
												
		$dbresult = db_select('mailwire_sender', 's')
		  ->fields('s')
			->orderBy('s.weight')
			->execute();
		foreach ($dbresult as $record) {
			$senders[] = $record->class_name;
		}
				
		if (count($senders)==0) {
			$sender = new DefaultMailSystem();
			return $sender->mail($message);
		}
		else {
			$retval = TRUE;
			foreach ($senders as $sender_class) {
				$sender = new $sender_class();
				$success = $sender->mail($message);
				if (!$success) $retval = FALSE; 
			}
			return $retval;
		}
  }
  
  /**
   * Find the calling module and key.
   * 
   * @return
   *   A stdClass:
   *   @code
   *   $ret->module  // the name of the module
   *   $ret->key     // the mail key
   *   @endcode
   */
  private function findCallingModule() {
  	$backtrace = debug_backtrace();
  	for ($i = 0; $i<count($backtrace); $i++) {
    	if ($backtrace[$i]['function']=='drupal_mail') {
    		// get the module and key
    		$ret->module = $backtrace[$i]['args'][0];
    		$ret->key = $backtrace[$i]['args'][1];
    		return $ret;
    	}
		}
  	_mailwire_log('Unable to determine the calling module and key! Module dependent mail formatters and senders are not called!', WATCHDOG_ERROR);
  	return NULL;
  }
  
}
